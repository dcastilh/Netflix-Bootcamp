package com.nttdata.persistence.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.nttdata.persistence.entity.SeasonEntity;

@Repository
public interface SeasonRepository extends PagingAndSortingRepository<SeasonEntity, Long> {

	List<SeasonEntity> findByTvShowId(Long tvShowId);

}

