package com.nttdata.controller.rest;

import java.util.List;

import com.nttdata.controller.rest.model.NetflixResponse;
import com.nttdata.controller.rest.model.SeasonRest;
import com.nttdata.exception.NetflixException;

public interface SeasonControllerRest {

	NetflixResponse<List<SeasonRest>> getSeasonsByTvShow(Long tvShowId) throws NetflixException;

}
