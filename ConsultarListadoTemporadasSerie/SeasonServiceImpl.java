package com.nttdata.service.impl;


import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nttdata.controller.rest.model.SeasonRest;
import com.nttdata.exception.NetflixException;
import com.nttdata.persistence.repository.SeasonRepository;
import com.nttdata.service.SeasonService;


@Service
public class SeasonServiceImpl implements SeasonService {

	@Autowired
	private SeasonRepository seasonRepository;

    @Autowired
    private ModelMapper modelMapper;
    
	@Override
	public List<SeasonRest> getSeasonsByTvShow(Long tvShowId) throws NetflixException {
		return seasonRepository.findByTvShowId(tvShowId).stream()
				.map(season -> modelMapper.map(season, SeasonRest.class)).collect(Collectors.toList());
	}

    
}
