
package com.nttdata.controller.rest.impl;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.nttdata.controller.rest.SeasonControllerRest;
import com.nttdata.controller.rest.model.NetflixResponse;
import com.nttdata.controller.rest.model.SeasonRest;
import com.nttdata.exception.NetflixException;
import com.nttdata.service.SeasonService;
import com.nttdata.util.constant.CommonConstantsUtils;
import com.nttdata.util.constant.RestConstantsUtils;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@Tag(name = "Seasons", description = "Season rest")
public class SeasonControllerRestImpl implements SeasonControllerRest {

	@Autowired
	private SeasonService seasonService;

	@Override
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = RestConstantsUtils.RESOURCE_SEASONS, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "getSeasonByTvShow", description = "Get all season by given TvShowId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
	public NetflixResponse<List<SeasonRest>> getSeasonsByTvShow(Long tvShowId) throws NetflixException {
		return new NetflixResponse<>(CommonConstantsUtils.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstantsUtils.OK,
				seasonService.getSeasonsByTvShow(tvShowId));
	}
	
}
