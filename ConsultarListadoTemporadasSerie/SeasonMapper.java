package com.nttdata.persistence.mapper;

import java.io.Serializable;

public interface SeasonMapper<E extends Serializable, D extends Serializable> {

	E mapToEntity(D dto);

	D mapToDto(E entity);

}
