package com.nttdata.service;

import java.util.List;

import com.nttdata.controller.rest.model.SeasonRest;
import com.nttdata.exception.NetflixException;

public interface SeasonService {
	
	List<SeasonRest> getSeasonsByTvShow(Long tvShowId) throws NetflixException;

}
