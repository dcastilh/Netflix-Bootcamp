package com.nttdata.persistence.mapper;

import java.io.Serializable;

public interface ChapterMapper<E extends Serializable, D extends Serializable> {

	E mapToEntity(D dto);

	D mapToDto(E entity);

}
