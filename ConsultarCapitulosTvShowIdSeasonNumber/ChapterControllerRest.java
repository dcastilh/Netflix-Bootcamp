package com.nttdata.controller.rest;

import java.util.List;

import com.nttdata.controller.rest.model.ChapterRest;
import com.nttdata.controller.rest.model.NetflixResponse;
import com.nttdata.exception.NetflixException;

public interface ChapterControllerRest {

	NetflixResponse<List<ChapterRest>> getChaptersByTvShowIdAndSeasonNumber(Long tvShowId, short seasonNumber)
			throws NetflixException;
	
}
