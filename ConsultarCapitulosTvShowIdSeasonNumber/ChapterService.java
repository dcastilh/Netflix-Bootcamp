package com.nttdata.service;

import java.util.List;

import com.nttdata.controller.rest.model.ChapterRest;
import com.nttdata.exception.NetflixException;

public interface ChapterService {

	List<ChapterRest> getChaptersByTvShowIdAndSeasonNumber(Long tvShowId, short seasonNumber) throws NetflixException;
	
}
