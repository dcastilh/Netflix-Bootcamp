
package com.nttdata.controller.rest.impl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.nttdata.controller.rest.ChapterControllerRest;
import com.nttdata.controller.rest.model.ChapterRest;
import com.nttdata.controller.rest.model.NetflixResponse;
import com.nttdata.exception.NetflixException;
import com.nttdata.service.ChapterService;
import com.nttdata.util.constant.CommonConstantsUtils;
import com.nttdata.util.constant.RestConstantsUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@Tag(name = "Chapters", description = "Chapter rest")
public class ChapterControllerRestImpl implements ChapterControllerRest {

	@Autowired
	private ChapterService chapterService;

	@Override
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = RestConstantsUtils.RESOURCE_CHAPTERTVSHOWIDSEASONID + RestConstantsUtils.RESOURCE_CHAPTER, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "getChaptersByTvShowIdAndSeasonNumber", description = "Get chapters by given TvShowId and SeasonNumber")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
	public NetflixResponse<List<ChapterRest>> getChaptersByTvShowIdAndSeasonNumber(@PathVariable Long tvShowId,
			@PathVariable short seasonNumber) throws NetflixException {
		return new NetflixResponse<>(CommonConstantsUtils.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstantsUtils.OK,
				chapterService.getChaptersByTvShowIdAndSeasonNumber(tvShowId, seasonNumber));
	}

	@Override
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = RestConstantsUtils.RESOURCE_CHAPTERTVSHOWIDSEASONIDCHAPTER  + RestConstantsUtils.RESOURCE_CHAPTER, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "getChapterByTvShowIdAndSeasonNumberAndChapterNumber", description = "Get chapters by given TvShowId, SeasonNumber and ChapterNumber")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
	public NetflixResponse<ChapterRest> getChapterByTvShowIdAndSeasonNumberAndChapterNumber(@PathVariable Long tvShowId,
			@PathVariable short seasonNumber, @PathVariable short number) throws NetflixException {
		return new NetflixResponse<>(CommonConstantsUtils.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstantsUtils.OK,
				chapterService.getChapterByTvShowIdAndSeasonNumberAndChapterNumber(tvShowId, seasonNumber, number));
	}
	
}
