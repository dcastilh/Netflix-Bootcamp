package com.nttdata.persistence.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.nttdata.persistence.entity.ChapterEntity;

@Repository
public interface ChapterRepository extends PagingAndSortingRepository<ChapterEntity, Long> {
	
	List<ChapterEntity> findBySeasonTvShowIdAndSeasonNumber(Long tvShowId, short seasonNumber);

	Optional<ChapterEntity> findBySeasonTvShowIdAndSeasonNumberAndNumber(Long tvShowId, short seasonNumber,
			short chapterNumber);
	
}
