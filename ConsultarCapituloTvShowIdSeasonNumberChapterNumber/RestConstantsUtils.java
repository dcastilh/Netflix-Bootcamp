package com.nttdata.util.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RestConstantsUtils {

	public final String APPLICATION_NAME = "/netflix";
	public final String API_VERSION_1 = "/v1";
	
	public final String SUCCESS = "Success";
	public final String OK = "OK";
	public final String CATEGORYID = "categoryId";
	
	public final String RESOURCE_CATEGORYID = "/{categoryId}";
	public static final String RESOURCE_ID = "/{id}";
	public static final String RESOURCE_TVNAME = "/{name}";
	public static final String RESOURCE_NUMBER = "/{number}";

	
	public final String RESOURCE_CATEGORY = "/category";
	public final String RESOURCE_TVSHOW = "/tvshow";
	public final String RESOURCE_SEASONS= "/season";
	public final String RESOURCE_CHAPTER= "/chapter";
	public final String RESOURCE_CHAPTERTVSHOWIDSEASONID = "/tv-show/{tvShowId}/season/{seasonNumber}";
	public final String RESOURCE_CHAPTERTVSHOWIDSEASONIDCHAPTER = "/tv-show/{tvShowId}/season/{seasonNumber}/chapter/{number}";

	
	
	public final String RESOURCE_CATEGORIESS = "/categories";

}
