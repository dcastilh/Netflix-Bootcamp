package com.nttdata.controller.rest.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.nttdata.controller.rest.TvShowControllerRest;
import com.nttdata.controller.rest.model.NetflixResponse;
import com.nttdata.controller.rest.model.TvShowRest;
import com.nttdata.exception.NetflixException;
import com.nttdata.service.TvShowService;
import com.nttdata.util.constant.CommonConstantsUtils;
import com.nttdata.util.constant.RestConstantsUtils;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@Tag(name = "TvShows", description = "TvShow Rest")
@RequestMapping(RestConstantsUtils.RESOURCE_TVSHOW)
public class TvShowControllerRestImpl implements TvShowControllerRest {

	@Autowired
    private TvShowService tvShowService;

	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = RestConstantsUtils.RESOURCE_TVSHOW, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "getTvShowByCategoryId", description = "Get TvShow by given category id")
	@ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
	public NetflixResponse<List<TvShowRest>> getTvShowsByCategory(@RequestParam Long categoryId)
			throws NetflixException, NotFoundException {
		return new NetflixResponse<>(HttpStatus.OK.toString(), String.valueOf(HttpStatus.OK.value()),
				CommonConstantsUtils.OK, tvShowService.getTvShowsByCategory(categoryId));
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = RestConstantsUtils.RESOURCE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "getTvShowById", description = "Get TvShow by given id")
	@ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
	public NetflixResponse<TvShowRest> getTvShowById(@PathVariable Long id) throws NetflixException, NotFoundException {
		return new NetflixResponse<>(HttpStatus.OK.toString(), String.valueOf(HttpStatus.OK.value()),
				CommonConstantsUtils.OK, tvShowService.getTvShowById(id));
	}
	
	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = RestConstantsUtils.RESOURCE_TVNAME, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "getTvShowByName", description = "Get TvShow by given name")
	@ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })	public NetflixResponse<TvShowRest> getTvShowByName(@PathVariable String name) throws NetflixException, NotFoundException {
		return new NetflixResponse<>(HttpStatus.OK.toString(), String.valueOf(HttpStatus.OK.value()),
				CommonConstantsUtils.OK, tvShowService.getTvShowByName(name));
	}
}
