package com.nttdata.controller.rest.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TvShowRest implements Serializable{
	
    private static final long serialVersionUID = 1L;
    
    @JsonProperty("id")
    private Long id;
    
    @JsonProperty("name")
	private String name;
    
    @JsonProperty("shortDescription")
	private String shortDescription;
    
    @JsonProperty("longDescription")
	private String longDescription;
    
    @JsonProperty("year")
	private int year;
    
    @JsonProperty("recommendedAge")
	private byte recommendedAge;
    
    @JsonProperty("advertising")
	private String advertising;
    
}
