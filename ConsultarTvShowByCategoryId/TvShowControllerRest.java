package com.nttdata.controller.rest;

import java.util.List;

import com.nttdata.controller.rest.model.NetflixResponse;
import com.nttdata.controller.rest.model.TvShowRest;
import com.nttdata.exception.NetflixException;

import javassist.NotFoundException;

public interface TvShowControllerRest {

	NetflixResponse<List<TvShowRest>> getTvShowsByCategory(Long categoryId) throws NetflixException, NotFoundException;

}
