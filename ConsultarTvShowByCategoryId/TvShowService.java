package com.nttdata.service;

import java.util.List;

import com.nttdata.controller.rest.model.TvShowRest;
import com.nttdata.exception.NetflixException;

import javassist.NotFoundException;

public interface TvShowService {
	
	List<TvShowRest> getTvShowsByCategory(Long categoryId) throws NetflixException, NotFoundException;

}
