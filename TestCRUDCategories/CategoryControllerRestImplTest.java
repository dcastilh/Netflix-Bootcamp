package com.nttdata.controllers.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Matchers.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;

import com.nttdata.persistence.entity.CategoryEntity;
import com.nttdata.controller.rest.impl.CategoryControllerRestImpl;
import com.nttdata.controller.rest.model.NetflixResponse;
import com.nttdata.controller.rest.model.CategoryRest;
import com.nttdata.exception.NetflixException;
import com.nttdata.service.CategoryService;
import com.nttdata.util.constant.RestConstantsUtils;

@RunWith(MockitoJUnitRunner.class)
public class CategoryControllerRestImplTest {

	static final Long ID = 1L;
	static final CategoryEntity CATEGORY_ENTITY = new CategoryEntity();
	static final CategoryRest CATEGORY_REST = new CategoryRest();

    @Mock
    private CategoryService categoryService;

    @InjectMocks
    private CategoryControllerRestImpl categoryControllerRestImpl;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		CATEGORY_ENTITY.setId(ID);
		CATEGORY_REST.setId(ID);
	}

    @Test
    public void getAllCategorysTest() throws NetflixException {
		// given
		int page = 0;
		int size = 2;
		Pageable pageable = PageRequest.of(page, size);
		HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
		PagedResourcesAssembler<CategoryRest> assembler = new PagedResourcesAssembler<>(resolver, null);

		CategoryRest categoryRest2 = new CategoryRest();
		categoryRest2.setId(2L);

		EntityModel<CategoryRest> categoryEntityModel1 = new EntityModel<>(CATEGORY_REST);
		EntityModel<CategoryRest> categoryEntityModel2 = new EntityModel<>(categoryRest2);

		Collection<EntityModel<CategoryRest>> categoryEntitiyModelCollection = new ArrayList<>();
		categoryEntitiyModelCollection.add(categoryEntityModel1);
		categoryEntitiyModelCollection.add(categoryEntityModel2);

		PagedModel<EntityModel<CategoryRest>> categorysPagedModel = new PagedModel<>(categoryEntitiyModelCollection,null);
		Mockito.when(categoryService.getAllCategorys(any(Pageable.class),any(PagedResourcesAssembler.class))).thenReturn(categorysPagedModel);

		// when
		NetflixResponse<PagedModel<EntityModel<CategoryRest>>> response = categoryControllerRestImpl.getAllCategorys(page,size,pageable,assembler);

		// then
		assertNotNull(response);
		assertEquals(RestConstantsUtils.OK, response.getMessage());
		assertNotNull(response.getData().getContent());
	}

    @Test
    public void getCategoryByIdTest() throws NetflixException {
		Mockito.when(categoryService.getCategoryById(anyLong())).thenReturn(CATEGORY_REST);

		NetflixResponse<CategoryRest> response = categoryControllerRestImpl.getCategoryById(ID);

		assertNotNull(response);
		assertEquals(String.valueOf(HttpStatus.OK), response.getStatus());
		assertEquals(RestConstantsUtils.OK, response.getMessage());
		assertEquals(CATEGORY_REST, response.getData());
	}

    @Test
    public void createCategoryTest() throws NetflixException {
		Mockito.when(categoryService.createCategory(any(CategoryRest.class))).thenReturn(CATEGORY_REST);

		NetflixResponse<CategoryRest> response = categoryControllerRestImpl.createCategory(CATEGORY_REST);

		assertNotNull(response);
		assertEquals(String.valueOf(HttpStatus.OK), response.getStatus());
		assertEquals("200", response.getCode());
		assertEquals(RestConstantsUtils.OK, response.getMessage());
		assertEquals(CATEGORY_REST, response.getData());
	}

    @Test
    public void deleteCategoryTest() throws NetflixException {
		categoryControllerRestImpl.deleteCategory(ID);

		Mockito.verify(categoryService, Mockito.times(1)).deleteCategory(Mockito.anyLong());

	}

    @Test
    public void updateCategoryTest() throws NetflixException {
		CategoryRest categoryRestModified = new CategoryRest();
		categoryRestModified.setId(2L);
		Mockito.when(categoryService.updateCategory(any(CategoryRest.class))).thenReturn(categoryRestModified);

		NetflixResponse<CategoryRest> response = categoryControllerRestImpl.updateCategory(CATEGORY_REST);

		assertNotNull(response);
		assertEquals(String.valueOf(HttpStatus.OK), response.getStatus());
		assertEquals("200" , response.getCode());
		assertEquals( RestConstantsUtils.OK, response.getMessage());
		assertEquals(categoryRestModified, response.getData());
		assertNotEquals(CATEGORY_REST,response.getData());
	}
}
