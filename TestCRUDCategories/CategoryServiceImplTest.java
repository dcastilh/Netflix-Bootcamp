package com.nttdata.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.*;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.nttdata.controller.rest.model.CategoryRest;
import com.nttdata.exception.NetflixException;
import com.nttdata.exception.NetflixNotFoundException;
import com.nttdata.persistence.entity.CategoryEntity;
import com.nttdata.persistence.repository.CategoryRepository;
import com.nttdata.service.impl.CategoryServiceImpl;

@RunWith(SpringRunner.class)
@WebAppConfiguration
public class CategoryServiceImplTest {

	static final Long ID = 1L;
	static final CategoryEntity CATEGORY_ENTITY = new CategoryEntity();
	static final CategoryRest CATEGORY_REST = new CategoryRest();

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private CategoryServiceImpl categoryService;

    @Before
    public void init() {
	 MockitoAnnotations.initMocks(this);
		CATEGORY_ENTITY.setId(ID);
		CATEGORY_REST.setId(ID);

		Mockito.when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(CATEGORY_ENTITY));
		Mockito.when(modelMapper.map(any(CategoryEntity.class), eq(CategoryRest.class))).thenReturn(CATEGORY_REST);
		Mockito.when(modelMapper.map(any(CategoryRest.class),eq(CategoryEntity.class))).thenReturn(CATEGORY_ENTITY);

	}

    @Test
    public void getAllCategorysTest() throws NetflixException {
		Pageable pageable = PageRequest.of(0, 1);
		HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
		PagedResourcesAssembler<CategoryRest> assembler = new PagedResourcesAssembler<>(resolver, null);

		Page<CategoryEntity> categoryPage = new PageImpl<>(List.of(CATEGORY_ENTITY), pageable, 0);
		Mockito.when(categoryRepository.findAll(any(Pageable.class))).thenReturn(categoryPage);

		PagedModel<EntityModel<CategoryRest>> pagedModel = categoryService.getAllCategorys(pageable, assembler);

		assertNotNull(pagedModel);
    }

    @Test
    public void getCategoryByIdTest() throws NetflixException {
		CategoryRest response = categoryService.getCategoryById(ID);

		assertEquals(ID,response.getId());
    }

    @Test
    public void createCategoryTest() throws NetflixException {
		Mockito.when(categoryRepository.save(CATEGORY_ENTITY)).thenReturn(CATEGORY_ENTITY);

		CategoryRest categoryRestOut = categoryService.createCategory(CATEGORY_REST);

		assertEquals(CATEGORY_REST,categoryRestOut);
		Mockito.verify(categoryRepository, Mockito.times(1)).save(Mockito.any(CategoryEntity.class));
    }

    @Test
    public void updateCategoryTest() throws NetflixException {
		Mockito.when(categoryRepository.save(any())).thenReturn(CATEGORY_ENTITY);

		CategoryRest categoryRestOut = categoryService.updateCategory(CATEGORY_REST);
		assertEquals(CATEGORY_REST,categoryRestOut);
		Mockito.verify(categoryRepository, Mockito.times(1)).save(Mockito.any(CategoryEntity.class));
    }

    @Test(expected = NetflixNotFoundException.class)
    public void updateCategoryButNotExists() throws NetflixException {

		Mockito.when(categoryRepository.findById(ID)).thenReturn(Optional.empty());

		categoryService.updateCategory(CATEGORY_REST);
	}

    @Test
    public void deleteCategory() throws NetflixException {

		categoryService.deleteCategory(1L);

		Mockito.verify(categoryRepository, Mockito.times(1)).deleteById(anyLong());
	}
}