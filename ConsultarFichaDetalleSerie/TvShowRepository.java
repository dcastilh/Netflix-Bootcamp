package com.nttdata.persistence.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.nttdata.persistence.entity.TvShowEntity;

@Repository
public interface TvShowRepository extends PagingAndSortingRepository<TvShowEntity, Long> {

	List<TvShowEntity> findByCategoryId(Long categoryId);

	TvShowEntity findByName(String name);

}

