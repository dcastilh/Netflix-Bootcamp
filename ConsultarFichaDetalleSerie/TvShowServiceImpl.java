package com.nttdata.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nttdata.controller.rest.model.TvShowRest;
import com.nttdata.exception.NetflixException;
import com.nttdata.persistence.repository.TvShowRepository;
import com.nttdata.service.TvShowService;

import javassist.NotFoundException;

@Service
public class TvShowServiceImpl implements TvShowService {

    @Autowired
    TvShowRepository tvShowRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
	public List<TvShowRest> getTvShowsByCategory(Long categoryId) throws NetflixException, NotFoundException {
		try {
			tvShowRepository.findById(categoryId).get();
		} catch (EntityNotFoundException entityNotFoundException) {
			throw new NotFoundException(entityNotFoundException.getMessage());
		}
		return tvShowRepository.findByCategoryId(categoryId).stream()
				.map(tvShow -> modelMapper.map(tvShow, TvShowRest.class)).collect(Collectors.toList());

	}

    @Override
	public TvShowRest getTvShowById(Long id) throws NetflixException, NotFoundException {
		try {
			tvShowRepository.findById(id).get();
		} catch (EntityNotFoundException entityNotFoundException) {
			throw new NotFoundException(entityNotFoundException.getMessage());
		}
		return modelMapper.map(tvShowRepository.findById(id).get(), TvShowRest.class);
	}
	
	@Override
	public TvShowRest getTvShowByName(String name) throws NetflixException, NotFoundException {
		try {
			tvShowRepository.findByName(name);
		} catch (EntityNotFoundException entityNotFoundException) {
			throw new NotFoundException(entityNotFoundException.getMessage());
		}
		return modelMapper.map(tvShowRepository.findByName(name), TvShowRest.class);
	}
	
}
