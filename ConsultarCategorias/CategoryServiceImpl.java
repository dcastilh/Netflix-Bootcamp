package com.nttdata.service.impl;
 
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;

import com.nttdata.controller.rest.model.CategoryRest;
import com.nttdata.exception.NetflixException;
import com.nttdata.exception.NetflixNotFoundException;
import com.nttdata.exception.error.ErrorDto;
import com.nttdata.persistence.entity.CategoryEntity;
import com.nttdata.persistence.repository.CategoryRepository;
import com.nttdata.service.CategoryService;
import com.nttdata.util.constant.ExceptionConstantsUtils;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public PagedModel<EntityModel<CategoryRest>> getAllCategorys(final Pageable pageable,
	    final PagedResourcesAssembler<CategoryRest> assembler) throws NetflixException {
	Page<CategoryEntity> categoryPage = categoryRepository.findAll(pageable);
	Page<CategoryRest> categoryRestList = categoryPage.map(category -> modelMapper.map(category, CategoryRest.class));
	return assembler.toModel(categoryRestList);
    }

    @Override
    public CategoryRest createCategory(final CategoryRest categoryRest) throws NetflixException {
	CategoryEntity categoryEntity = modelMapper.map(categoryRest, CategoryEntity.class);
	return modelMapper.map(categoryRepository.save(categoryEntity), CategoryRest.class);
    }

    @Override
    public CategoryRest getCategoryById(final Long id) throws NetflixException {
	CategoryEntity category = categoryRepository.findById(id)
		.orElseThrow(() -> new NetflixNotFoundException(new ErrorDto(ExceptionConstantsUtils.NOT_FOUND_GENERIC)));
	return modelMapper.map(category, CategoryRest.class);
    }

    @Override
    public CategoryRest updateCategory(final CategoryRest categoryRest) throws NetflixException {
	CategoryEntity category = categoryRepository.findById(categoryRest.getId())
		.orElseThrow(() -> new NetflixNotFoundException(new ErrorDto(ExceptionConstantsUtils.NOT_FOUND_GENERIC)));

	return modelMapper.map(categoryRepository.save(updateCategory(category, categoryRest)), CategoryRest.class);
    }

    @Override
    public void deleteCategory(final Long id) throws NetflixException {
	categoryRepository.deleteById(id);
    }

    private CategoryEntity updateCategory(final CategoryEntity category, final CategoryRest categoryRest) {
	if (categoryRest.getName() != null) {
	    category.setName(categoryRest.getName());
	}

	return category;
    }
}
