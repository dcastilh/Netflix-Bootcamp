
package com.nttdata.controller.rest.impl;
 
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.nttdata.controller.rest.CategoryControllerRest;
import com.nttdata.controller.rest.model.NetflixResponse;
import com.nttdata.controller.rest.model.CategoryRest;
import com.nttdata.exception.NetflixException;
import com.nttdata.service.CategoryService;
import com.nttdata.util.constant.CommonConstantsUtils;
import com.nttdata.util.constant.RestConstantsUtils;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@Tag(name = "Categories", description = "Category rest")
public class CategoryControllerRestImpl implements CategoryControllerRest {

    @Autowired
    private CategoryService categoryService;

    @Override
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = RestConstantsUtils.RESOURCE_CATEGORY, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "getAllCategorys", description = "Get all Categorys paginated")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
    public NetflixResponse<PagedModel<EntityModel<CategoryRest>>> getAllCategorys(
      @RequestParam(defaultValue = CommonConstantsUtils.ZERO) final int page,
	    @RequestParam(defaultValue = CommonConstantsUtils.TWENTY) final int size, 
      @Parameter(hidden = true) final Pageable pageable,
	    @Parameter(hidden = true) final PagedResourcesAssembler<CategoryRest> assembler) throws NetflixException {
	return new NetflixResponse<>(HttpStatus.OK.toString(), String.valueOf(HttpStatus.OK.value()),
		CommonConstantsUtils.OK,categoryService.getAllCategorys(pageable, assembler));
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "getCategoryById", description = "Get one Category by given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content)
    })
    @GetMapping(value = RestConstantsUtils.RESOURCE_CATEGORY + RestConstantsUtils.RESOURCE_CATEGORYID)
    public NetflixResponse<CategoryRest> getCategoryById(@PathVariable(value = RestConstantsUtils.CATEGORYID) final Long id)
	    throws NetflixException {
	return new NetflixResponse<>(HttpStatus.OK.toString(), String.valueOf(HttpStatus.OK.value()),
		CommonConstantsUtils.OK,categoryService.getCategoryById(id));
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = RestConstantsUtils.RESOURCE_CATEGORY)
    @Operation(summary = "CategoryClient", description = "Create a new Category")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
    public NetflixResponse<CategoryRest> createCategory(@RequestBody final CategoryRest category) throws NetflixException {
	return new NetflixResponse<>(HttpStatus.OK.toString(), String.valueOf(HttpStatus.OK.value()),
		CommonConstantsUtils.OK,categoryService.createCategory(category));
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "updateCategoryStatus", description = "Update Category status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content)
    })
    @PatchMapping(value = RestConstantsUtils.RESOURCE_CATEGORY)
    public NetflixResponse<CategoryRest> updateCategory(@RequestBody final CategoryRest categoryRest) throws NetflixException {
	return new NetflixResponse<>(HttpStatus.OK.toString(), String.valueOf(HttpStatus.OK.value()),
		CommonConstantsUtils.OK,categoryService.updateCategory(categoryRest));
    }

    @Override
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "deleteCategory", description = "Delete Category by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "No Content"),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "Forbidden")
    })
    @DeleteMapping(value = RestConstantsUtils.RESOURCE_CATEGORY + RestConstantsUtils.RESOURCE_CATEGORYID)
    public void deleteCategory(@PathVariable(value = RestConstantsUtils.CATEGORYID) final Long id)
	    throws NetflixException {
	      categoryService.deleteCategory(id);
    }
}
